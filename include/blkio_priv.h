#ifndef BLKIO_PRIV_H
#define BLKIO_PRIV_H

#include <sys/types.h>

struct blkio_t {
  char *name;
  int bd;
  off_t offs;
  //ssize_t offs;
};

size_t blkio_get_kernel_sector_size(struct blkio_t *dev);
off_t blkio_get_kernel_media_size(struct blkio_t *dev);

int blkio_kernel_sync(struct blkio_t *dev);

#endif
