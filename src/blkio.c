#define _XOPEN_SOURCE 500

#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <blkio.h>
#include <blkio_priv.h>

blkio_t blkio_open(char *name, blkio_flags_t flags)
{
  blkio_t dev;

  if (!name)
    return NULL;

  dev = malloc(sizeof(struct blkio_t));
  if (!dev)
    return NULL;

  dev->name = strdup(name);
  if (!dev->name)
    return NULL;

  dev->bd = open(name, O_RDWR);
  if (dev->bd < 0)
    return NULL;

  dev->offs = 0;

  return dev;
}

int blkio_close(blkio_t dev)
{
  if (!dev)
    return -1;

  if (!close(dev->bd))
    return -1;

  free(dev->name);
  free(dev);

  return 0;
}

int blkio_read(blkio_t dev, void *buf, size_t sz)
{
//  dev->read();
  pread(dev->bd, buf, sz, dev->offs);
  dev->offs += sz;

  return 0;
}

int blkio_write(blkio_t dev, void *buf, size_t sz)
{
  pwrite(dev->bd, buf, sz, dev->offs);
  dev->offs += sz;

  return 0;
}

int blkio_seek(blkio_t dev, off_t off, int whence)
{
  off_t ret;

  ret = lseek(dev->bd, off, whence);
  if (ret < 0)
    return -1;

  dev->offs = ret;

  return 0;
}

off_t blkio_get_media_size(blkio_t dev)
{
  off_t dev_size = 0;
  struct stat st;

  dev_size = blkio_get_kernel_media_size(dev);
  if (dev_size)
    return dev_size;

  /* Regular files or devices under the Hurd.  */
  if (fstat(dev->bd, &st) == 0)
    return st.st_size;

  return 0;
}

size_t blkio_get_sector_size(blkio_t dev)
{
  int sect_size;

  sect_size = blkio_get_kernel_sector_size(dev);
  if (sect_size)
    return sect_size;

  return 512;
}

int blkio_sync(blkio_t dev)
{
  if (blkio_kernel_sync(dev))
    return 0;

  sync();

  return 0;
}

int blkio_reread_partitions(blkio_t dev)
{
  if (blkio_kernel_reread_partitions(dev))
    return 0;

  return 0;
}
