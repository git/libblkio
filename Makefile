#
# libblkio Makefile
#

CFLAGS = -Wall -Wextra -g
MK_CFLAGS = -D_REENTRANT -Iinclude/ -fvisibility=hidden

LIBNAME = libblkio
SOVERSION_MAJOR = 0
SOVERSION_MINOR = 0

SOVERSION = $(SOVERSION_MAJOR).$(SOVERSION_MINOR)
SONAME = $(LIBNAME).so.$(SOVERSION)
STNAME = $(LIBNAME).a

SRCS := blkio.c part.c size.c sync.c
#SRCS := $(addprefix src/,$(SRCS))

SO_OBJS = $(SRCS:.c=.lo)
ST_OBJS = $(SRCS:.c=.o)

KERNEL := $(shell uname -s | tr '[:upper:]' '[:lower:]')

VPATH = src/$(KERNEL):src/generic:src

%.lo: %.c
	$(CC) $(CFLAGS) $(MK_CFLAGS) -fPIC -c -o $@ $<

%.o: %.c
	$(CC) $(CFLAGS) $(MK_CFLAGS) -c -o $@ $<

build: $(SONAME) $(STNAME)

$(STNAME): $(ST_OBJS)
	$(AR) cru $@ $^

$(SONAME): $(SO_OBJS)
	$(CC) -shared \
	  -Wl,-z,defs \
	  -Wl,-soname -Wl,$(SONAME) \
	  -Wl,--version-script=$(LIBNAME).map \
	  -fvisibility=hidden \
	  -o $@ $^

install: build
	mkdir -p $(DESTDIR)/usr/include
	mkdir -p $(DESTDIR)/usr/lib
	mkdir -p $(DESTDIR)/lib
	install -m644 include/blkio.h $(DESTDIR)/usr/include/
	install -s -m755 $(SONAME) $(DESTDIR)/lib/
	install -m755 $(STNAME) $(DESTDIR)/usr/lib/
	ln -s /lib/$(LIBNAME).$(SOVERSION_MAJOR) $(DESTDIR)/usr/lib/$(LIBNAME).so
	ln -s $(SONAME) $(DESTDIR)/lib/$(LIBNAME).$(SOVERSION_MAJOR)

clean:
	$(RM) $(SO_OBJS)
	$(RM) $(ST_OBJS)
	$(RM) $(SONAME)
	$(RM) $(STNAME)
