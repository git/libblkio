#ifndef BLKIO_H
#define BLKIO_H

#include <sys/types.h>

#ifdef __GNUC__
#define API_PUBLIC __attribute__((visibility("default")))
#else
#define API_PUBLIC
#endif

typedef enum {
  blkio_flag_read,
  blkio_flag_write,
  blkio_flag_create,
} blkio_flags_t;

typedef struct blkio_t *blkio_t;

blkio_t blkio_open(char *name, blkio_flags_t flags) API_PUBLIC;
int blkio_close(blkio_t dev) API_PUBLIC;
int blkio_read(blkio_t dev, void *buf, size_t sz) API_PUBLIC;
int blkio_write(blkio_t dev, void *buf, size_t sz) API_PUBLIC;
int blkio_seek(blkio_t dev, off_t off, int whence) API_PUBLIC;

off_t blkio_get_media_size(blkio_t dev) API_PUBLIC;
size_t blkio_get_sector_size(blkio_t dev) API_PUBLIC;

int blkio_sync(blkio_t dev) API_PUBLIC;
int blkio_reread_partitions(blkio_t dev) API_PUBLIC;

#endif
